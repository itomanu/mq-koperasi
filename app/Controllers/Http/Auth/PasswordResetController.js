'use strict'

const { validate, validateAll } = use('Validator')
const User = use('App/Models/User')
const PasswordReset = use('App/Models/PasswordReset')
const randomString = require('random-string')
const Mail = use('Mail')
const Hash = use('Hash')

class PasswordResetController {
  reset({ view }) {
    return view.render('auth.passwords.reset')
  }

  async send({ request, session, response }) {
    // validate input
    const validation = await validate(request.only('email'), {
      email: 'required|email'
    }, {
      'email.required'    : 'Email tidak boleh kosong',
      'email.email'       : 'Email tidak valid',
    })

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll()

      return response.redirect('back')
    }

    try {
      const user = await User.findBy('email', request.input('email'))

      await PasswordReset.query().where('email', user.email).delete()

      const { token } = await PasswordReset.create({
        email: user.email,
        token: randomString({ length: 40 })
      })

      const mailData = {
        user: user.toJSON(),
        token
      }

      await Mail.send('auth.emails.password_reset', mailData, message => {
        message.to(user.email)
        .from('hello@adonisjs.com')
        .subject('Reset Password')
      })

      session.flash({
        notification: {
          type: 'success',
          message: 'Kami telah mengirimkan email untuk konfirmasi reset password, mohon konfirmasi link pada alamat email Anda.'
        }
      })
  
      return response.redirect('back')
    } catch (error) {
      session.flash({
        notification: {
          type: 'danger',
          message: 'Mohon maaf, user tidak ditemukan.'
        }
      })
      return response.redirect('back')
    }
  }

  resetForm ({ params, view }) {
    return view.render('auth.passwords.change', { token: params.token })
  }

  async changePassword ({ request, session, response }) {
    const validation = await validateAll(request.all(), {
      token: 'required',
      email: 'required|email',
      password: 'required|confirmed'
    }, {
      'token.required'    : 'Token tidak valid',
      'email.required'    : 'Email tidak boleh kosong',
      'email.email'       : 'Email tidak valid',
    })

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashExcept(['password', 'password_confirmation'])

      return response.redirect('back')
    }

    try {
      const user = await User.findBy('email', request.input('email'))

      const token = await PasswordReset.query()
        .where('email', user.email)
        .where('token', request.input('token'))
        .first()

        if (!token) {
          session.flash({
            notification: {
              type: 'danger',
              message: 'Token Reset Password tidak valid.'
            }
          })
          return response.redirect('back')
        }

        user.password = request.input('password')
        await user.save()

        await PasswordReset.query().where('email', user.email).delete()

        session.flash({
          notification: {
            type: 'success',
            message: 'Password Anda telah berhasil diganti.'
          }
        })
    
        return response.redirect('/signin')
    } catch (error) {
      session.flash({
        notification: {
          type: 'danger',
          message: 'Mohon maaf, user tidak ditemukan.'
        }
      })
      return response.redirect('back')
    }
  }
}

module.exports = PasswordResetController
