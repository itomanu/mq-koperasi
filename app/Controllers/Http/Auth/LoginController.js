'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')

class LoginController {
  signin({ auth, view, response }) {
    return view.render('auth.signin')
  }

  async login({ request, auth, session, response }) {
    // get form data
    const { username, password, remember } = request.all()
    
    // retrieve user base on the form data
    const user = await User.query()
      .where('username', username)
      .where('is_active', true)
      .first()
    
    // verify password
    if (user) {
      const passwordVerified = await Hash.verify(password, user.password)

      if (passwordVerified) {
        // login user
        await auth.remember(!!remember).login(user)

        return response.route('home')
      }
    }

    // display error
    session.flash({
      notification: {
        type: 'danger',
        message: 'Proses login tidak dapat dilakukan. Pastikan alamat email Anda telah terkonfirmasi.'
      }
    })

    return response.redirect('back')
  }
}

module.exports = LoginController
