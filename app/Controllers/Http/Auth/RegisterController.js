'use strict'

const { validateAll } = use('Validator')
const User = use('App/Models/User')
const randomString = require('random-string')
const Mail = use('Mail')

class RegisterController {
  signup({ view }) {
    return view.render('auth.signup')
  }

  async register({ request, session, response }) {
    // validate input
    const validation = await validateAll(request.all(), {
      username: 'required|unique:users,username',
      email: 'required|email|unique:users,email',
      password: 'required'
    }, {
      'username.required' : 'Username harus diisi',
      'email.required'    : 'Email harus diisi',
      'email.email'       : 'Email tidak valid',
      'password.required' : 'Password tidak boleh kosong'
    })

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashExcept(['password'])

      return response.redirect('back')
    }

    // create user
    const user = await User.create({
      username: request.input('username'),
      email: request.input('email'),
      password: request.input('password'),
      confirmation_token: randomString({ length: 40 })
    })
    
    // send email
    await Mail.send('auth.emails.confirm_email', user.toJSON(), message => {
      message.to(user.email)
      .from('hello@adonisjs.com')
      .subject('Mohon konfirmasi email Anda')
    })
    
    // display success
    session.flash({
      notification: {
        type: 'success',
        message: 'Pendaftaran berhasil! Kami telah mengirimkan email untuk konfirmasi, mohon konfirmasi link pada alamat email Anda.'
      }
    })

    return response.redirect('back')
  }

  async confirmEmail({ params, session, response }) {
    // get user with the confirm token
    const user = await User.findBy('confirmation_token', params.token)

    // set confirm to null and is_active to true
    user.confirmation_token = null
    user.is_active = true

    // persist user to database
    await user.save()
    
    // display success
    session.flash({
      notification: {
        type: 'success',
        message: 'Email Anda telah berhasil dikonfirmasi.'
      }
    })

    return response.redirect('/signin')
  }
}

module.exports = RegisterController
