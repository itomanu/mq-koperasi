'use strict'

class AuthenticatedController {
  async signout({ auth, response }) {
    await auth.logout()
    return response.redirect('/signin')
  }
}

module.exports = AuthenticatedController
