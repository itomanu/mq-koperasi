'use strict'

class HomeController {
  dashboard({ auth, view, response }) {
    return view.render('home', { active: 'dashboard' })
  }
}

module.exports = HomeController
