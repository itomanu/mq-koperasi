# Merqurion Koperasi

## How To Run

1. Run `npm install` to install all dependencies
2. Make a copy of `.env.example` rename it to `.env`
3. Run `adonis key:generate` to generate the secret key
4. Run `adonis migration:run` to setup the database
5. Run `adonis serve --dev` to run the application
