'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route')

Route.get('/', 'Menu/HomeController.dashboard').as('home').middleware(['auth'])

// Register
Route.get('signup', 'Auth/RegisterController.signup').middleware(['authenticated'])
Route.post('register', 'Auth/RegisterController.register').as('register')
Route.get('register/confirm/:token', 'Auth/RegisterController.confirmEmail')

// Login
Route.get('signin', 'Auth/LoginController.signin').as('signin').middleware(['authenticated'])
Route.post('login', 'Auth/LoginController.login').as('login')

// Logout
Route.get('signout', 'Auth/AuthenticatedController.signout')

// Reset Password
Route.get('password/reset', 'Auth/PasswordResetController.reset')
Route.post('password/email', 'Auth/PasswordResetController.send').as('reset')
Route.get('password/reset/:token', 'Auth/PasswordResetController.resetForm')
Route.post('password/reset', 'Auth/PasswordResetController.changePassword').as('changePassword')